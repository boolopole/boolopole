<?php
require_once('vendor/autoload.php');
DataDogStatsD::increment('login-callback');
# /js-login.php
$fb = new Facebook\Facebook([
  'app_id' => '1856322541309076',
  'app_secret' => '0e4dd498b6f697ea376b3ca935e8947b',
  'default_graph_version' => 'v2.2',
  ]);

$helper = $fb->getJavaScriptHelper();

try {
    $console = 'Startlogincallback ';
    // Get the access token
    $accessToken = $helper->getAccessToken();
    $permissions = array(
        'email',
        'name'
    );
    $res = $fb->get('/me?fields=name,email', $accessToken);
    $out = $res->getDecodedBody();
    $_SESSION['name'] = $out['name'];
    $console = $console.$out["name"].' ';
    if (isset($_SESSION['email'])) { 
      $_SESSION['email'] = $out['email'];
    } else { 
      $_SESSION['email'] = 'empty';
    }
    $_SESSION['id'] = $out['id'];
    $_SESSION['level'] *= 2;
    $conn = new mysqli( USER_DB_HOST , USER_DB_USER , USER_DB_PSWD , USER_DB_NAME );
    // Check connection

    if ($conn->connect_error) {
        $console = $console.'conectionerror ';
        die("Connection failed: " . $conn->connect_error);
    }
    $_SESSION['name'] = str_replace('"','', $_SESSION['name']);
    $_SESSION['name'] = str_replace("'","", $_SESSION['name']);
    $_SESSION['name'] = str_replace('<','', $_SESSION['name']);
    $_SESSION['name'] = str_replace('>','', $_SESSION['name']);
    $_SESSION['name'] = str_replace('[','', $_SESSION['name']);
    $_SESSION['name'] = str_replace(']','', $_SESSION['name']);
    $_SESSION['name'] = str_replace('{','', $_SESSION['name']);
    $_SESSION['name'] = str_replace('}','', $_SESSION['name']);
    $_SESSION['name'] = str_replace('(','', $_SESSION['name']);
    $_SESSION['name'] = str_replace(')','', $_SESSION['name']);
    $_SESSION['name'] = str_replace(';','', $_SESSION['name']);
    $_SESSION['name'] = str_replace(':','', $_SESSION['name']);
    $sql = "INSERT INTO `authorised_users` (`id`,`name`,`email`,`last_seen`) VALUES ('" . $_SESSION['id'] . "', '" . $_SESSION['name'] . "', '" . $_SESSION['email'] . "', NOW()) ON DUPLICATE KEY UPDATE last_seen = NOW()";
    $console = $console.$sql.' ';
    if ($conn->query($sql) === TRUE) {
        $console = $console.'sqlsuccess ';
        echo "";
    } else {
        $console = "Error saving details" . $conn->error;
    }

    // Check Authorised
    $sql2 = "select * from authorised_users where id='" . $_SESSION['id'] . "'";
    $result = $conn->query($sql2);
    $output = $result->fetch_assoc();
    $level   = $output['level'];
    $_SESSION['tgid'] = $output['tgid'];
    $_SESSION['disid'] = $output['disid'];
    if (isset($level)) {
        if ($level >= 1) { $_SESSION['level'] *= 3; } //basic authorisation
        if ($level >= 2) { $_SESSION['level'] *= 7; } //spec authorisation
        if ($level >= 3) { $_SESSION['level'] *= 5; } //admin authorisation
        $console = $console.'authed ';
    } else {
        $page = 'unauth';
        $console = $console.'unauthed ';
    }
    // Check if donor
    $_SESSION['donor'] = FALSE;
    if ($output['donor'] == 1) {
        $_SESSION['donor'] = TRUE;
    }
    if (isset($output['donor_date'])) {
        if (strtotime($output['donor_date']) > strtotime('now')) {
            $_SESSION['donor'] = TRUE;
        }
    }
    if (isset($output['donor_date_override'])) {
        if (strtotime($output['donor_date_override']) > strtotime('now')) {
            $_SESSION['donor'] = TRUE;
        }
    }
    $conn->close();




} catch(Facebook\Exceptions\FacebookSDKException $e) {
    // There was an error communicating with Graph
    echo '<script>console.log("e1 '.$console.'");</script>';
    echo '<script>console.log("e2 '.$e->getMessage().'");</script>';
    $console = $console.$e->getMessage();
    $page = 'login';
    unset($_SESSION['name']);
}

//redirect to $page

?>
