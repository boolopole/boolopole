<?php
if (!isset($_SESSION))
  {
    session_start();
  }
session_destroy();
if (isset($_SESSION)) {
  $helper = array_keys($_SESSION);
  foreach ($helper as $key){
    unset($_SESSION[$key]);
  }
}
setcookie('tester', 'hi', 1);
setcookie('session', 'dead', 1);
unset($_COOKIE['session']);
unset($_COOKIE['tester']);
include_once('config.php');
header( 'Location: https://'.SITE_ADDRESS.'/logout' );

?>
